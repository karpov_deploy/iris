import mlflow
import numpy as np

class iris_model_wrapper(mlflow.pyfunc.PythonModel):
    '''
    input:
    - model
    - threshold_0_1 и threshold_1_2 — коэффициенты из /get_iris_thresholds
    logic:
    predict_float — исходное числовое предсказание модели
    predict_string — значение, которое получается из class путём сравнения с thresholds: 
        setosa < threshold_0_1 < versicolor < threshold_1_2
    return:
    {"class": <float>, "class_str": <setosa|versicolor|virginica>, "threshold_0_1": <float>, "threshold_1_2": <float>}
    '''
    
    def __init__(self, model):
        self.model = model

    def predict(self, model_input: np.array):
        # get thresholds
        # format: {"threshold_0_1": <float>, "threshold_1_2": <float>}
        url = "http://95.216.215.127:5020/get_iris_thresholds"
        thresholds = requests.get(url).json()

        # get model predict
        predict_float = self.model.predict(model_input).tolist()[0]
        if predict_float < thresholds['threshold_0_1']:
            predict_string = 'setosa'
        elif predict_float < thresholds['threshold_1_2']:
            predict_string = 'versicolor'
        else:
            predict_string = 'virginica'
        
        out_dict = {
            "class": predict_float, 
            "class_str": predict_string, 
            "threshold_0_1": thresholds['threshold_0_1'], 
            "threshold_1_2": thresholds['threshold_1_2'],
            }

        return out_dict