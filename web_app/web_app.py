import os
import flask
from flask import jsonify, request, Response
import requests

import mlflow
import mlflow.sklearn
import numpy as np

app = flask.Flask(__name__)

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://95.216.215.127:19001"
os.environ["MLFLOW_TRACKING_URI"] = "http://95.216.215.127:5030"
os.environ["AWS_ACCESS_KEY_ID"] = "IAM_ACCESS_KEY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"

# load models
MODEL_PATH_DEFAULT = "models:/iris_model/production"
model = mlflow.sklearn.load_model(MODEL_PATH_DEFAULT)


@app.route('/get_source_iris_pred', methods=['GET'])
def return_predict_float():
    sepal_length = request.args.get('sepal_length', default = 1.0, type=float)
    sepal_width = request.args.get('sepal_width', default = 1.0, type=float)
    predict = model.predict(np.array([[sepal_length, sepal_width]])).tolist()[0]
    return jsonify({"prediction": predict})

@app.route('/get_string_iris_pred', methods=['GET'])
def return_predict_string():
    return Response(501, status=501, mimetype='application/json')


if __name__ == "__main__":
    app.run()
else:
    application = app